import React from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';
import Home from './pages/Home';
import Portfolio from './pages/Portfolio';
import Contact from './pages/Contact';
import { Project1, Project2, Project3, Project4 } from './pages/Projects';

const App = () => {
    return (
        <div>
            <Routes>
                <Route path="/" element={<Home />}></Route>
                <Route path="/portfolio" element={<Portfolio />}></Route>
                <Route path="/project-1" element={<Project1 />}></Route>
                <Route path="/project-2" element={<Project2 />}></Route>
                <Route path="/project-3" element={<Project3 />}></Route>
                <Route path="/project-4" element={<Project4 />}></Route>
                <Route path="/contact" element={<Contact />}></Route>
                <Route path="*" element={<Navigate replace to="/" />} />
            </Routes>
        </div>
    );
};

export default App;