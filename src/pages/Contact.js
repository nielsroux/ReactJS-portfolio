import React from 'react';
import { CopyToClipboard } from 'react-copy-to-clipboard/lib/Component';
import ButtonsBottom from '../components/ButtonsBottom';
import ContactForm from '../components/ContactForm';
import Logo from '../components/Logo';
import Mouse from '../components/Mouse';
import Navigation from '../components/Navigation';
import SocialNetworks from '../components/SocialNetworks';

const Contact = () => {
    return (
        <main>
            <Mouse />
            <div className="contact">
                <Navigation />
                <Logo />
                <ContactForm />
                <div className="contact-infos">
                    <div className="address">
                        <div className="content">
                            <h4>adresse</h4>
                            <p>5 rue damiette</p>
                            <p>76000 Rouen</p>
                        </div>
                    </div>
                    <div className="phone">
                        <div className="content">
                            <h4>Phone</h4>
                            <CopyToClipboard text="0631688410" className="hover">
                                <p style={{cursor: 'pointer'}} className="clipboard" onClick={() =>{
                                    alert('Phone copié !');
                                }}>06 31 68 84 10</p>
                            </CopyToClipboard>
                        </div>
                    </div>
                    <div className="email">
                        <div className="content">
                            <h4>Email</h4>
                            <CopyToClipboard text="nielsroux@gmail.com" className="hover">
                                <p style={{cursor: 'pointer'}} className="clipboard" onClick={() =>{
                                    alert('Email copié !');
                                }}>nielsroux@gmail.com</p>
                            </CopyToClipboard>
                        </div>
                    </div>
                    <SocialNetworks />
                    <div className="credits">
                        <p>Slein - {new Date().getFullYear()}</p>
                    </div>
                </div>
                <ButtonsBottom left={'/project-4'} />
            </div>
        </main>
    );
};

export default Contact;