import React from 'react';
import Mouse from '../components/Mouse';

const Portfolio = () => {
    return (
        <main>
            <Mouse />
            Portfolio
        </main>
    );
};

export default Portfolio;