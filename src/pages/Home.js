import React from 'react';
import ButtonsBottom from '../components/ButtonsBottom';
import DynamicText from '../components/DynamicText';
import Mouse from '../components/Mouse';
import Navigation from '../components/Navigation';
import SocialNetworks from '../components/SocialNetworks';

const Home = () => {
    return (
        <main>
            <Mouse />
            <div className="home">
                <div>
                    <Navigation />
                    <SocialNetworks />
                    <div className="home-main">
                        <div className="main-content">
                            <h1>Slein</h1>
                            <h2><DynamicText /></h2>
                        </div>
                    </div>
                </div>
                <ButtonsBottom right={"/project-1"} />
            </div>
        </main>
    );
};

export default Home;